let arr = ["travel", "hello", "eat", "ski", "lift"];
let count = 0;
let sumLength = arr.forEach(function (elem) {
  if (elem.length > 3) {
    count++;
  }
});
console.log(count);

let people = [
  { name: "Іван", age: 25, sex: "чоловіча", favSport: "swimming" },
  { name: "Yana", age: 26, sex: "жіноча", favSport: "swimming" },
  { name: "Yaroslav", age: 20, sex: "чоловіча", favSport: "swimming" },
  { name: "Karina", age: 20, sex: "жіноча", favSport: "swimming" },
  { name: "Vova", age: 22, sex: "чоловіча", favSport: "swimming" },
];

let test = people.filter(function (elem) {
  return elem.sex === "чоловіча";
});
console.log(test);

let array = ["hello", "world", 23, "23", null];

function filterBy(arr, oper) {
  let arrFilter = [];
  arrFilter = arr.filter(function(elem){
    return typeof(elem) === oper
  })
  return arrFilter
}
console.log(filterBy(array, "number"));
